FROM ubuntu

ADD jdk8.tar.gz /opt

RUN mv /opt/jdk1.8.0_181 /opt/jdk8

ENV PATH="$PATH:/opt/jdk8/bin"
ENV JAVA_HOME="/opt/jdk8"
ENV JRE_HOME="/opt/jdk8/jre"
ENV JENKINS_HOME="/apps/jenkins" 

RUN mkdir -p /apps/jenkins/jobs && useradd -d /apps/jenkins/ -s /bin/bash  jenkins

COPY jdk8.sh /etc/profile.d/

RUN chmod +x /etc/profile.d/jdk8.sh

COPY jenkins.war   /apps/jenkins/

RUN chown -R jenkins:jenkins /apps/jenkins/


EXPOSE 8080 50000


USER jenkins
#VOLUME /apps/jenkins/.jenkins/secrets/initialAdminPassword
#VOLUME /apps/jenkins/

CMD /opt/jdk8/bin/java  -jar /apps/jenkins/jenkins.war

